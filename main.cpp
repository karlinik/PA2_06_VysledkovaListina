#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <stack>
#include <queue>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */



/*======================================================*/
class OrderingDoesNotExistException
{
};

class DuplicateMatchException
{
};
/*======================================================*/
struct Node {
	Node(string name):m_name(name){}
	Node(const Node &other) :m_name(other.m_name) {
		for (auto it1 = other.in.begin(); it1 != other.in.end(); it1++)
		{
			in.push_back((*it1));
		}
		for (auto it2 = other.out.begin(); it2 != other.out.end(); it2++)
			out.push_back((*it2));
	}
	vector<Node*>in;
	vector<Node*>out;
	string m_name;
	bool operator ==(const Node *x) const{
		return m_name == x->m_name;
	}
};
/*======================================================*/
class Graph
{
	int V;
	vector<Node*>vertex;
	list<Node*>next;
	list<string>result;
public:
	Graph(int V, vector<string>players);
	~Graph();
	void AddEdge(string w, string l);
	void TopologicalSort(); 
	void Next();
	list<string> GetResult() const{
		return result;
	}
	void Print();
	void PrintNext();
};
/*======================================================*/
Graph::Graph(int V,vector<string>players):V(V)
{
	for (auto it = players.begin(); it != players.end(); it++)
	{
		Node* tmp = new Node(*it);
		vertex.push_back(tmp);
	}
}
//--------------------------------------------------------
Graph::~Graph() {
	for (auto it1 = vertex.begin(); it1 != vertex.end(); it1++)
		delete *it1;
}
//--------------------------------------------------------
void Graph::AddEdge(string w, string l)
{
	vector<Node*>::iterator winner, loser;
	auto first = vertex.begin();
	auto last = vertex.end();
	while (first != last) {
		if ((*first)->m_name == w) winner= first;
		if ((*first)->m_name == l) loser=first;
		++first;
	}
	(*winner)->out.push_back((*loser));
	(*loser)->in.push_back((*winner));
}
//--------------------------------------------------------
void Graph::Next() {
	for (auto it = vertex.begin(); it != vertex.end(); it++)
	{
		if ((*it)->in.size() == 0)
		{
			Node* tmp = new Node(*(*it));
			next.push_back(tmp);
		}
	}
}
//--------------------------------------------------------
void Graph::TopologicalSort()
{
	Next();
	int cnt = 0; // Initialize count of visited vertices

	while (!next.empty())
	{
		if (next.size() > 1)
			throw OrderingDoesNotExistException();
		result.push_back(next.front()->m_name);
		cnt++;
		for (auto it = next.front()->out.begin(); it != next.front()->out.end(); it++)
		{
			if ((*it)->in.size() == 1)
				next.push_back((*it));

			for (auto iter = (*it)->in.begin(); iter != (*it)->in.end();)
			{
				if ((*iter)->m_name == next.front()->m_name) {
					iter = (*it)->in.erase(iter);
					break;
				}
				else iter++;
			}
		}
		next.pop_front();
	}

	// Check if there was a cycle
	if (cnt != V)
	{
		throw OrderingDoesNotExistException();
	}
}
//--------------------------------------------------------
void Graph::Print() {
	for (auto it = vertex.begin(); it != vertex.end(); it++)
	{
		cout << (*it)->m_name << " : in: " << (*it)->in.size() <<" : out: "<< (*it)->out.size() << endl;
	}
}
//--------------------------------------------------------
void Graph::PrintNext() {
	cout << "size next: " << next.size() << endl;
	for (auto it = next.begin(); it != next.end(); it++)
	{
		cout << (*it)->m_name << " | ";
	}
	cout << endl;
}
/*======================================================*/

struct PairComparator {
	bool operator () (pair<string, string> pp1, pair<string, string> pp2) const
	{
		return (pp1.first < pp2.first ||
			(pp1.first == pp2.first && pp1.second < pp2.second));
	}
};
/*======================================================*/
template <typename _M>
class CContest
{
public:
	CContest() {}// default constructor
	~CContest() {}// destructor
	//-----------------------------------------------------
	CContest& AddMatch(string contestant1, string contestant2, _M result) {
		auto it1 = lower_bound(m_players.begin(), m_players.end(), contestant1);
		if (it1 == m_players.end() || (*it1) != contestant1)
			m_players.insert(it1, contestant1);
		it1 = lower_bound(m_players.begin(), m_players.end(), contestant2);
		if (it1 == m_players.end() || (*it1) != contestant2)
			m_players.insert(it1, contestant2);
		
		auto iter = m_matches.find(make_pair(contestant1, contestant2));
		if(iter!=m_matches.end())
			throw DuplicateMatchException();

		iter = m_matches.find(make_pair(contestant2, contestant1));
		if (iter != m_matches.end())
			throw DuplicateMatchException();
		m_matches.insert(make_pair(make_pair(contestant1, contestant2), result));
		
		return *this;
	}
	//-----------------------------------------------------
	template<typename _F>
	bool IsOrdered(_F comp) const {
		Graph g(m_players.size(),m_players);
		//add edges
		for (auto it = m_matches.begin(); it != m_matches.end(); it++)
		{
			int result = comp(it->second);
			if (result>0) 
				g.AddEdge(it->first.first, it->first.second); 
			else if (result == 0) 
				return false;
			else 
				g.AddEdge(it->first.second, it->first.first);
		}
		try {
			g.TopologicalSort();
		}
		catch ( const OrderingDoesNotExistException &e){
			return false;
		}
		return true;
	}
	//-----------------------------------------------------
	template<typename _F>
	list<string> Results(_F comp) const{
		list<string>res;
		Graph g(m_players.size(), m_players);
		//add edges
		for (auto it = m_matches.begin(); it != m_matches.end(); it++)
		{
			int result = comp(it->second);
			
			if (result>0) 
				g.AddEdge(it->first.first, it->first.second);
			else if (result == 0) 
				throw OrderingDoesNotExistException();
			else 
				g.AddEdge(it->first.second, it->first.first);
		}
		try {
			g.TopologicalSort();
		}
		catch (const OrderingDoesNotExistException &e) {
			throw;
		}
		res = g.GetResult();
		return res;
	}
	//-----------------------------------------------------
	void Print() const{
		cout << "player:" << endl;
		for (auto it1 = m_players.begin(); it1 != m_players.end(); it1++) {
			cout << (*it1) << endl;
		}
		cout << "matches:" << endl;
		for (auto it2 = m_matches.begin(); it2 != m_matches.end(); it2++) {
			cout << it2->first.first <<" "<< it2->first.second << endl;
		}
	}
	//-----------------------------------------------------
private:
	map<pair<string, string>, _M,PairComparator>m_matches;
	vector<string>m_players;
};
#ifndef __PROGTEST__
struct CMatch
{
public:
	CMatch(int               a,
		int               b)
		: m_A(a),
		m_B(b)
	{
	}

	int                      m_A;
	int                      m_B;
};

class HigherScoreThreshold
{
public:
	HigherScoreThreshold(int diffAtLeast)
		: m_DiffAtLeast(diffAtLeast)
	{
	}
	int                      operator ()                   (const CMatch & x) const
	{
		return (x.m_A > x.m_B + m_DiffAtLeast) - (x.m_B > x.m_A + m_DiffAtLeast);
	}
private:
	int            m_DiffAtLeast;
};

int                HigherScore(const CMatch    & x)
{
	return (x.m_A > x.m_B) - (x.m_B > x.m_A);
}

int                main(void)
{
	CContest<CMatch>  x;

	x.AddMatch("C++", "Pascal", CMatch(10, 3))
		.AddMatch("C++", "Java", CMatch(8, 1))
		.AddMatch("Pascal", "Basic", CMatch(40, 0))
		.AddMatch("Java", "PHP", CMatch(6, 2))
		.AddMatch("Java", "Pascal", CMatch(7, 3))
		.AddMatch("PHP", "Basic", CMatch(10, 0));


	assert(!x.IsOrdered(HigherScore));
	try
	{
		list<string> res = x.Results(HigherScore);
		assert("Exception missing!" == NULL);
	}
	catch (const OrderingDoesNotExistException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown!" == NULL);
	}

	x.AddMatch("PHP", "Pascal", CMatch(3, 6));

	assert(x.IsOrdered(HigherScore));
	try
	{
		list<string> res = x.Results(HigherScore);
		assert((res == list<string>{ "C++", "Java", "Pascal", "PHP", "Basic" }));
	}
	catch (...)
	{
		assert("Unexpected exception!" == NULL);
	}


	assert(!x.IsOrdered(HigherScoreThreshold(3)));
	try
	{
		list<string> res = x.Results(HigherScoreThreshold(3));
		assert("Exception missing!" == NULL);
	}
	catch (const OrderingDoesNotExistException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown!" == NULL);
	}

	assert(x.IsOrdered([](const CMatch & x)
	{
		return (x.m_A < x.m_B) - (x.m_B < x.m_A);
	}));
	try
	{
		list<string> res = x.Results([](const CMatch & x)
		{
			return (x.m_A < x.m_B) - (x.m_B < x.m_A);
		});
		assert((res == list<string>{ "Basic", "PHP", "Pascal", "Java", "C++" }));
	}
	catch (...)
	{
		assert("Unexpected exception!" == NULL);
	}

	CContest<bool>  y;

	y.AddMatch("Python", "PHP", true)
		.AddMatch("PHP", "Perl", true)
		.AddMatch("Perl", "Bash", true)
		.AddMatch("Bash", "JavaScript", true)
		.AddMatch("JavaScript", "VBScript", true);

	assert(y.IsOrdered([](bool v)
	{
		return v ? 10 : -10;
	}));
	try
	{
		list<string> res = y.Results([](bool v)
		{
			return v ? 10 : -10;
		});
		assert((res == list<string>{ "Python", "PHP", "Perl", "Bash", "JavaScript", "VBScript" }));
	}
	catch (...)
	{
		assert("Unexpected exception!" == NULL);
	}

	y.AddMatch("PHP", "JavaScript", false);
	assert(!y.IsOrdered([](bool v)
	{
		return v ? 10 : -10;
	}));
	try
	{
		list<string> res = y.Results([](bool v)
		{
			return v ? 10 : -10;
		});
		assert("Exception missing!" == NULL);
	}
	catch (const OrderingDoesNotExistException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown!" == NULL);
	}

	try
	{
		y.AddMatch("PHP", "JavaScript", false);
		assert("Exception missing!" == NULL);
	}
	catch (const DuplicateMatchException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown!" == NULL);
	}

	try
	{
		y.AddMatch("JavaScript", "PHP", true);
		assert("Exception missing!" == NULL);
	}
	catch (const DuplicateMatchException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown!" == NULL);
	}
	return 0;
}
#endif /* __PROGTEST__ */